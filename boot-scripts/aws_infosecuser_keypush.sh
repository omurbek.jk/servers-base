#!/bin/bash
############################################################################################################
#Program: infosecuser_keypush_aws.sh
#Author: Prajwal Panchmahalkar [ppanchmahalkar@vmware.com]
#Execution(As root user): sh ./aws_infosecuser_keypush.sh
#Credits: Devesh Pandey (dpandey@vmware.com), Ashutosh Jain (jashutosh@vmware.com) for assistance.
#Date: June 24th 2016
#version: 0.4 
#Change log: added AWS key, added AllowUsers Directive 
# Contact redteam@vmware.com if you have any issues running the script.
############################################################################################################

public_key="ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDPoiSWadmCvmwbhJkmsu7jp96E/1x5/pylw1fT+3x6IU74aeXsQTPTzgYK7jYzbrpylzshPqu/FK6S6uQga5gV4xcl3IudTndwjLEpBDEXTjCVQgrPIjLyACZ8bTQ51zqDaUTIpJomFFHOcwt1/+fvXA9nSg9DJRFnPxn2suyUmkB3EfU0ifks64n0fO5HFrNTD99aTele//gu+05sJKJA9AA7jAj7P6G6Jpn9883m6UpbuDHUQoMnmCL4VRDTi5n74iI2lBvRVjfTJzLfDo2h3ck5LR684YcW5Vf6PdFPmz0yo6HXJJuULnEJ4VwJ0F3yyrKlAIYCTiQ6+dlwbTUP InfosecUserAWS"

checkroot_infosec()
{
	#Check if the script is running Super User
	echo "[!]Checking if the script is running as root user"
        usr_name=`id -u`
        if [ "$usr_name" -ne "0" ]; then
        	echo "[-]This script must run as root user. Force exiting the script "
                exit 1
        fi
}
makesudo_infosec()
{
	usermod -a -G sudo $1
	echo "[+]Added infosecuser to sudo group"
	checkpermissions_infosec
}

editsudoerfile()
{
	check_sudoerfile=$(cat /etc/sudoers | grep -o infosecuser)
	if [ "${check_sudoerfile}" = "infosecuser" ]; then
		echo "[*]infosecuser is already added to sudo group"
		ifauthkeys_Exist
	else
		echo "[!]Making a Copy of sudoer file, in case of file corruption"
		cp /etc/sudoers /etc/sudoers.old
		echo "[+]Adding infosecuser to /etc/sudoer"
		echo 'infosecuser ALL=(ALL) ALL' >> /etc/sudoers
		echo "[*]infosecuser is now added to sudo group"
		ifauthkeys_Exist
	fi

}


checkpermissions_infosec()
{
	echo "[-]Disabling password for infosec user "
	passwd -d infosecuser
	su_users=$(getent group sudo)
	if [ ${#su_users} -le 1 ]; then 
		echo "[*] sudo group does not exist editing the sudoer file"
		editsudoerfile
	else
		check_var=$(echo $su_users | grep -o infosecuser)
		if [ "${check_var}" = "infosecuser" ]; then
			echo "[*]infosecuser is already added to sudo group"
			ifauthkeys_Exist
		else
			echo '[-]infosecuser not added to sudo group. Adding it'
			makesudo_infosec infosecuser
		fi
	fi
}


createuser_infosec()
{
	useradd -m $1
	if [ $? -ne 0 ]; then
		echo "[-]Problem in creating user, force exiting the script"
		exit 1
	fi
	#sudo passwd -d $1
	echo "[-]Disabling password for infosec user "
	passwd -d $1
	check_sudoer_group=$(getent group sudoer)
	verify_sudoer_group=`echo $check_sudoer_group | grep -o sudoer`
	if [ "${verify_sudoer_group}" = "sudoer" ]; then
		echo "[+]sudoer group already exists . Adding user infosecuser to the group "
                #sudo usermod -a -G sudoer $1
		usermod -a -G sudoer $1
		echo "[+]added infosecuser to sudoer group "
	else
		echo "[!]sudoer group does not exist. Creating it"
		#sudo groupadd sudoer
		groupadd sudoer
		#sudo usermod -a -G sudoer $1
		usermod -a -G sudoer $1
		echo "[*]Added infosecuser to sudoer group"
	fi
	if [ $? -ne 0 ]; then
		echo "[-]Problem in assigning group to infosecuser. Force exiting the script"
		exit 1
	fi
	checkpermissions_infosec

}

ifauthkeys_Exist()
{
	if [ -f "/home/infosecuser/.ssh/authorized_keys" ]; then
		echo "[*]infosec user authorized keys file exists already"
		{
			echo "$public_key" > /home/infosecuser/.ssh/authorized_keys
			#curl -f http://10.113.82.123/id_rsa.pub -o /home/infosecuser/.ssh/authorized_keys
		} ||
		{
			echo "[-]Key download failed. Exiting! Contact redteam@vmware.com for assistance"
			exit 1
		}
		echo "[+]Public Key downloaded and pushed in the authorized keys file"
		echo "[!]Changing ownership of the authorized key file to infosecuser "
		chown infosecuser /home/infosecuser/.ssh/authorized_keys
	    echo "[+]Providing 600 permissions to the authorized key file "
		chmod 600 /home/infosecuser/.ssh/authorized_keys         
	else
		echo "[-]infosec user authorized keys file does not exist"
		mkdir /home/infosecuser/.ssh
		chown -R infosecuser /home/infosecuser/.ssh
		{
			echo "$public_key" > /home/infosecuser/.ssh/authorized_keys
			#curl -f http://10.113.82.123/id_rsa.pub -o /home/infosecuser/.ssh/authorized_keys
		} ||
		{
			echo "[-]Key download failed. Exiting! Contact redteam@vmware.com for assistance" 
			exit 1
		}
        echo "[!]Changing ownership of the authorized key file to infosecuser "
		chown infosecuser /home/infosecuser/.ssh/authorized_keys
		echo "[+]Providing 600 permissions to the authorized key file "
		chmod 600 /home/infosecuser/.ssh/authorized_keys
	fi
}

checkuser_infosec()
{
	#check if infosecuser already exists if not then create one
    if ! id -u $1 > /dev/null 2>&1; then
		echo "[!]infosec user does not exist . Creating it "
		createuser_infosec infosecuser
	else
		echo "[!]Infosecuser already exists, checking for sudo permissions"
        checkpermissions_infosec
    fi
}

check_sshconfig()
{
	configFile="/etc/ssh/sshd_config"
	if grep -q "AllowUsers" "$configFile"; then
		echo "AllowUsers infosecuser" >> $configFile
	fi
}

# subroutines end
echo "[$]Starting the process"
set +e
checkroot_infosec
set -e
echo "[+]Checking infosecuser"
set +e
checkuser_infosec infosecuser
set -e
echo "[+]Script Completed "
set +e
check_sshconfig
set -e
#echo "testing the login"
#ssh -q -o "BatchMode=yes" -i id_rsa infosecuser@10.113.82.123 "echo 2>&1" && echo $host SSH_OK || echo $host SSH_NOK
exit 0
