#!/bin/sh

# Increase root filesystem to it's max capacity
if grep '/dev/sda1' /etc/mtab > /dev/null 2>&1; then
    echo "resizing /dev/sda1"
    resize2fs /dev/sda1
elif grep '/dev/xvda1' /etc/mtab > /dev/null 2>&1; then
    echo "resizing /dev/xvda1"
    resize2fs /dev/xvda1
fi

