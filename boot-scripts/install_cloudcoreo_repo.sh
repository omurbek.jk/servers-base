#!/bin/sh

## install the cloudcoreo yum repo
osType="$(gawk -F= '/^NAME/{print $2}' /etc/os-release)"

if echo "$osType" | grep -q -i -e "amazon" -e "centos" -e "rhel" -e "redhat"; then
    rpm -ivh https://s3.amazonaws.com/cloudcoreo-yum/repo/tools/cloudcoreo-repo-0.0.3-1.noarch.rpm 
    yum makecache
elif echo "$osType" | grep -q -i -e "ubuntu"; then
    apt-get update
fi
