#!/bin/bash

## if there is no datadog key supplied, exit
if [ -z "${DATADOG_KEY}" ]; then
    exit 0
fi

if [ "${DATADOG_KEY}" = 'None' ]; then
    exit 0
fi

DD_API_KEY="${DATADOG_KEY}" bash -c "$(curl -L https://raw.githubusercontent.com/DataDog/dd-agent/master/packaging/datadog-agent/source/install_agent.sh)"

sh -c "sed 's/api_key:.*/api_key: ${DATADOG_KEY}/' /etc/dd-agent/datadog.conf.example > /etc/dd-agent/datadog.conf"

/etc/init.d/datadog-agent restart
