#!/bin/bash

osType="$(gawk -F= '/^NAME/{print $2}' /etc/os-release)"

if echo "$osType" | grep -q -i -e "amazon" -e "centos" -e "rhel" -e "redhat"; then
    yum install -y screen
elif echo "$osType" | grep -q -i -e "ubuntu"; then
    apt-get install -y screen
fi


