#!/bin/sh

## make sure pip is installed
if echo "$osType" | grep -q -i -e "amazon" -e "centos" -e "rhel" -e "redhat"; then
    if ! rpm -qa | grep -q python-pip; then
	yum install -y python-pip
    fi
elif echo "$osType" | grep -q -i -e "ubuntu"; then
    if ! apt list --installed | grep -q python-pip; then
	apt-get -y python-pip
    fi
fi

## make sure we have the latest boto installed
pip install boto --upgrade
pip install boto3 --upgrade

## make sure we have filechunkio
if ! pip list | grep -q filechunkio; then
    pip install filechunkio --upgrade
fi

which python
ec=$?

if [ $ec = 0 ]; then
    echo "python is set up"
else
    echo "need to set python up"
    earliest_python="$(ls -lrta /usr/bin/ | grep -v "\-\>" | grep -P "^[^l].*python[\d\.]+$" | awk '{print $NF}' | sort | head -1)"
    ln -s /usr/bin/$earliest_python /usr/bin/python
fi
